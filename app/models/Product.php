<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once '../core/Model.php';
/**
*
*/
class Product extends Model
{
    public static function all(){ 
        //obtiene la conexion
        $db = Product:: db();
        //prepara la consulta
        $sql = "SELECT * FROM products";
        //ejecuta la consutl
        $statment = $db->query($sql);
       
        $users = $statment->fetchAll(PDO::FETCH_CLASS, Product::class);
        return $users;
    }
    public static function find($id){ 
        $db = Product::db();
        $stmt = $db->prepare('SELECT * FROM products WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Product::class);
        $user = $stmt->fetch(PDO::FETCH_CLASS);
        return $user;
    }
    public function insert()
    {
        $db = Product::db();
        $stmt = $db->prepare('INSERT INTO products(name, type_id,price) VALUES(:name, :type_id,:price)');
        $stmt->bindValue(':name', $this->name);
        $stmt->bindValue(':type_id', $this->type_id);
        $stmt->bindValue(':price', $this->price);
    
      
       
        return $stmt->execute();
    }
    public function delete()
    {
        $db = Product::db();
        $stmt = $db->prepare('DELETE FROM products WHERE id = :id');
        $stmt->bindValue(':id', $this->id);
        return $stmt->execute();
    }
    public function save()
    {
        $db = User::db();
        $stmt = $db->prepare('UPDATE products SET name = :name, type_id = :type_id, price = :price WHERE id = :id');
        $stmt->bindValue(':id', $this->id);
        $stmt->bindValue(':name', $this->name);
        $stmt->bindValue(':type_id', $this->type_id);
        $stmt->bindValue(':price', $this->price);
        
        return $stmt->execute();
    }
    public function type()
{
   
    $db = Product::db();
    $statement = $db->prepare('SELECT * FROM product_types WHERE id = :id');
    $statement->bindValue(':id', $this->type_id);
    $statement->execute();

    $statement->setFetchMode(PDO::FETCH_CLASS, Producttype::class);
    $user = $statement->fetch(PDO::FETCH_CLASS);

    return $user;
}
public function __get($atributoDesconocido)
{
    // return "atributo $atributoDesconocido desconocido";
    if (method_exists($this, $atributoDesconocido)) {
        $this->$atributoDesconocido = $this->$atributoDesconocido();
        return $this->$atributoDesconocido;
        // echo "<hr> atributo $x <hr>";
    } else {
        return;
    }
}
}
