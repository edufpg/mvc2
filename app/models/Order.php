<?php
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\OrderProducts;
require_once "../core/Model.php";
require_once "../app/models/User.php";
/**
*
*/
class Order extends Model
{
    function __construct()
    {

    }

    public static function all(){

        $db = Order::db();

        $statement = $db->query('SELECT * FROM orders');
        $orders = $statement->fetchAll(PDO::FETCH_CLASS,Order::class);
        return $orders;
    }

   
    public static function rowCount()
    {
        $db = Order::db();

        $statement = $db->prepare('SELECT count(id) as count FROM orders');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }

    public static function find($id){
        $db = Order::db();
        $statement = $db->prepare("SELECT * FROM orders WHERE id=:id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, Order::class);
        $order = $statement->fetch(PDO::FETCH_CLASS);
        return $order;
    }

    public function products(){
        $db = OrderProducts::db();
        $statement = $db->prepare('SELECT * FROM orders_products WHERE order_id = :id');
        $statement->bindValue(':id', $this->id);
        $statement->execute();
        $products = $statement->fetchAll(PDO::FETCH_CLASS, OrderProducts::class);

        return $products;
    }

    public static function findUserName($id){
        $db = User::db();
        $statement = $db->prepare("SELECT * FROM users WHERE id=:id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, User::class);
        $user = $statement->fetch(PDO::FETCH_CLASS);
        return $user->name . " " .$user->surname;
    }

    public function insert(){

        $db = Order::db();
        $statement = $db->prepare("INSERT INTO orders(date, user_id, price) VALUES(:date, :user_id, :price)");
        $statement->bindValue(":date", $this->date);
        $statement->bindValue(":user_id", $this->user_id);
        $statement->bindValue(":price", $this->price);
        $statement->execute();
        $this->id = $db->lastInsertId();
        return $this->id;
    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }

    }

    public function addproduct($product)
    {
        $db = Order::db();
        $statement = $db->prepare("INSERT INTO orders_products(order_id, product_id, price, quantity) VALUES(:order_id, :product_id, :price, :quantity)");
        $statement->bindValue(":order_id", $this->id);
        $statement->bindValue(":product_id", $product->id);
        $statement->bindValue(":price", $product->price);
        $statement->bindValue(":quantity", $product->quantity);

        $statement->execute();
    }
}
