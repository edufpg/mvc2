<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once '../core/Model.php';
/**
*
*/
class Producttype extends Model
{
    public static function all(){ 
        //obtiene la conexion
        $db = Producttype:: db();
        //prepara la consulta
        $sql = "SELECT * FROM product_types";
        //ejecuta la consutl
        $statment = $db->query($sql);
       
        $types = $statment->fetchAll(PDO::FETCH_CLASS, Producttype::class);
        return $types;
    }
    public static function find($id){ 
        $db = Producttype::db();
        $stmt = $db->prepare('SELECT * FROM product_types WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Producttype::class);
        $type = $stmt->fetch(PDO::FETCH_CLASS);
        return $type;
    }
    public function insert()
    {
        $db = Producttype::db();
        $stmt = $db->prepare('INSERT INTO product_types(name) VALUES(:name)');
        $stmt->bindValue(':name', $this->name);
      
        return $stmt->execute();
    }
   
        public function save()
        {
            $db = Producttype::db();
            $stmt = $db->prepare('UPDATE product_types SET name = :name WHERE id = :id');
            $stmt->bindValue(':id', $this->id);
            $stmt->bindValue(':name', $this->name);
          
            return $stmt->execute();
        }
    public function delete()
    {
        $db = Producttype::db();
        $stmt = $db->prepare('DELETE FROM product_types WHERE id = :id');
        $stmt->bindValue(':id', $this->id);
        return $stmt->execute();
    }
    public function products()
    {
       
       $db = Producttype::db();
        $statement = $db->prepare('SELECT * FROM products WHERE type_id = :type_id');
        $statement->bindValue(':type_id', $this->id);
        $statement->execute();
        $products = $statement->fetchAll(PDO::FETCH_CLASS ,Product::class);
    
       return $products;
    }
    public function __get($atributoDesconocido)
    {
    //  return "atributo $atributoDesconocido desconocido";
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            //  echo "<hr> atributo $x <hr>";
        } else {
            return;
    }
    }
}
