<?php
namespace App\Models;

use PDO;
use \Core\Model;

require_once "../core/Model.php";
/**
*
*/
class OrderProducts extends Model
{
    function __construct()
    {

    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
          
        } else {
            return "";
        }

    }
   

    public function findProductName()
    {
        $db = Product::db();
        $statement = $db->prepare("SELECT * FROM products WHERE id=:id");
        $statement->execute(array(":id" => $this->product_id));
        $statement->setFetchMode(PDO::FETCH_CLASS, Product::class);
        $product = $statement->fetch(PDO::FETCH_CLASS);
        return $product->name;
    }
    public function save()
    {
        $db = OrderProducts::db();
        $statement = $db->prepare("INSERT INTO orders_products(order_id, product_id, price, quantity) VALUES(:order_id, :product_id, :price, :quantity)");
        $statement->bindValue(":order_id", $this->order_id);
        $statement->bindValue(":product_id", $this->product_id);
        $statement->bindValue(":price", $this->price);
        $statement->bindValue(":quantity", $this->quantity);
        return $statement->execute();
    }

    
}
