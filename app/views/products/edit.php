
<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>


<h1>Edición de Producto</h1>

<form method="post" action="/product/login">
    <input type="hidden" name="id"
    value="<?php echo $user->id ?>">

<div class="form-group">
    <label>Nombre</label>
    <input type="text" name="name" class="form-control"
    value="<?php echo $user->name ?>"
    >
</div>
<div class="form-group">
    <label>Tipo de producto</label>
    <input type="text" name="type_id" class="form-control"
    value="<?php echo $user->type_id ?>"
    >
</div>

<div class="form-group">
    <label>Precio</label>
    <input type="text" name="price" class="form-control"
    value="<?php echo $user->price ?>"
    >
</div>
<div class="form-group">
  <select class="form-control" name="tipo">
  <?php foreach($types as $key => $type){?>
    <option  value="<?php echo $type->id ?>" <?php echo($user->type_id == $type->id ) ? 
       'selected="selected"' :'';
    ?>><?php echo $type->name ?></option>





    <?php  } ?>
  </select>
</div>
<button type="submit" class="btn btn-default">Enviar</button>
</form>

<?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
