<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de productos</h1>
      
      <a href="/product/create/" class="btn btn-primary">Crear Producto</a>
      <br></br>
      <table class="table table-striped table-hover">
        <tr>
          <td>Nombre</td>
          <td>Tipo de producto</td>
          <td>Precio</td>
          <td>Id</td>
        </tr>
      <?php foreach($users as $key => $user){?>
       
        <tr>
        <td  ><?php echo $user ->name; ?></td>
        <td  ><?php echo $user ->type->name; ?></td>
        <td  ><?php echo $user ->price; ?></td>
        <td ><?php echo $user ->id; ?></td>
        
        <td><a href="/product/delete/<?php echo $user ->id; ?>" class="btn btn-primary">Borrar Producto</a></td>
        <td><a href="/product/edit/<?php echo $user ->id; ?>" class="btn btn-primary">Modificar Producto</a></td>
        <td  ><a href="/product/show/<?php echo $user ->id ?>" class="btn btn-primary">Ver</a></td>
        <td  ><a class="btn btn-success" href="/order/sumar/<?php echo $p->id ?>">Añadir al carrito</a></td>



        </tr>



        
        <?php  } ?>
        </table>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
