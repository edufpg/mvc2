<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

<h1>Alta de usuario</h1>

<form method="post" action="/product/store">

<div class="form-group">
    <label>Nombre</label>
    <input type="text" name="name" class="form-control">
</div>
<!-- <div class="form-group">
    <label>Tipo de producto</label>
    <input type="text" name="type_id" class="form-control">
</div> -->

<div class="form-group">
    <label>Precio</label>
    <input type="text" name="price" class="form-control">
</div>
<div class="form-group">
  <select class="form-control" name="type_id">
  <?php foreach($types as $key => $type){?>
    <option  value="<?php echo $type->id ?>"><?php echo $type->name ?></option>
    <?php  } ?>
  </select>
</div>

<button type="submit" class="btn btn-default">Enviar</button>
</form>

<?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
