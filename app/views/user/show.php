<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Detalle de usuarios <?php echo $user->id?></h1>
        <ul>
        <li><strong>Nombre: </strong><?php echo $user->name?> </li>
        <li><strong>Apellidos: </strong><?php echo $user->surname?> </li>
        <li><strong>Fecha de nacimiento: </strong><?php echo $user->birthdate->format('d-m-Y');?> </li>
        <li><strong>Email: </strong><?php echo $user->email?> </li>



        </ul>
       
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
