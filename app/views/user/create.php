<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<head>
    <title>Nuevo Peñista</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      
        <form action="/user/store" method="post">
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input type="text" class="form-control" name="name" value="<?php echo $_SESSION['old']['name'] ?>">
            </div>
            <div class="form-group">
                <label for="surname">Apellidos:</label>
                <input type="text" class="form-control" name="surname" value="<?php echo $_SESSION['old']['surname'] ?>">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value="<?php echo $_SESSION['old']['email'] ?>">
            </div>
            <div class="form-group">
                <label for="birthdate">Fecha Nacimiento:</label>
                <input type="date" class="form-control" name="birthdate" placeholder="YYYY-mm-dd" value="<?php echo $_SESSION['old']['birthdate'] ?>">
            </div>
            <div class="form-group">
                <label for="password">Nueva Contraseña:</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="form-group">
                <label for="password">Repite Contraseña:</label>
                <input type="password" class="form-control" name="password2">
            </div>
            <h3 style="color: red;"><?php echo isset($message) && !empty($message) ? $message : "" ?></h3>
            <button type="submit" class="btn btn-default">Añadir Usuario</button>
        </form>

</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
