<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de usuarios</h1>
      <p class="lead">Página de presentación</p>
     
      <a href="/user/create/" class="btn btn-primary">Crear Usuario</a>
      <br></br>
      <table class="table table-striped table-hover">
      <tr>
          <td>Nombre</td>
          <td>Apellido</td>
          <td>Email</td>
          <td>Fecha de nacimiento</td>
        </tr>
      <?php foreach($users as $key => $user){?>
      
        <tr>
        <td ><?php echo $user ->name; ?></td>
        <td><?php echo $user ->surname; ?></td>
        <td><?php echo $user ->email; ?></td>
        <td><?php echo $user->birthdate ? $user->birthdate->format('d-m-Y'): 'nonato'; ?></td>
        <td><a href="/user/edit/<?php echo $user ->id; ?>" class="btn btn-primary">Modificar Usuario</a></td>
        <td><a href="/user/delete/<?php echo $user ->id; ?>" class="btn btn-primary">Borrar Usuario</a></td>
        <td ><a href="/user/show/<?php echo $user ->id ?>" class="btn btn-primary">Ver</a></td>
        

        </tr>



      
        <?php  } ?>
        </table>
        
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
