<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<head>
    <title>Nuevo Peñista</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <form action="/register/store" method="post">
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="form-group">
                <label for="apellidos">Apellidos:</label>
                <input type="text" class="form-control" name="apellidos">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="nacimiento">Fecha Nacimiento:</label>
                <input type="date" class="form-control" name="nacimiento" placeholder="YYYY-mm-dd">
            </div>
            <div class="form-group">
                <label for="password">Nueva Contraseña:</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="form-group">
                <label for="password">Repite Contraseña:</label>
                <input type="password" class="form-control" name="password2">
            </div>
            <h3 style="color: red;"><?php echo isset($message) && !empty($message) ? $message : "" ?></h3>
            <button type="submit" class="btn btn-default">Añadir Peñista</button>
        </form>
    </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
