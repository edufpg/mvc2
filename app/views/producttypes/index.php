<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Tipos de Productos</h1>
      <p class="lead">Página de presentación</p>
      <a href="/producttype/create/" class="btn btn-primary">Crear Tipo de producto</a>
      <br></br>
      <table class="table table-striped table-hover">
      <tr>
        <td>Id</td>
        <td>Nombre</td>
        <td>Productos asociados:</td>
      </tr>
      <?php foreach($types as $key => $type){?>
       
        <tr>
        <td ><?php echo $type ->id; ?></td>
        <td><?php echo $type ->name; ?></td>
        <td>
        <?php
        // var_dump($type->products); 
        // exit();
        foreach($type->products as $key => $product){?>
        <?php echo $product->name; ?>
      
        <?php  } ?></td>
        <td><a href="/producttype/delete/<?php echo $type ->id; ?>" class="btn btn-primary">Borrar Tipo de producto</a></td>
       
        <td><a href="/producttype/edit/<?php echo $type ->id; ?>" class="btn btn-primary">Modificar TDP</a></td>
        <td ><a href="/producttype/show/<?php echo $type ->id ?>" class="btn btn-primary">Ver</a></td>


        </tr>

        <?php  } ?>
        </table>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
