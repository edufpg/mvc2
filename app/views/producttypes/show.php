<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Detalle de tipo de producto <?php echo $type->id?></h1>
        <ul>
        <li><strong>Id del Tipo: </strong><?php echo $type->id?> </li>
        <li><strong>Tipo de producto: </strong><?php echo $type->name?> </li>
       <li><strong>Productos asociados: </strong><?php
        // var_dump($type->products); 
        // exit();
        foreach($type->products as $key => $product){?>
        <?php echo $product->name; ?>
      
        <?php  } ?></td></li>
        </ul>
       
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
