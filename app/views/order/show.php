<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <h1>Detalles del pedido</h1>

        <ul>
            <li><?php echo "Id: " . $order->id ?></li>
            <li><?php echo "Fecha: " .$order->date ?></li>
            <li><?php echo "Precio total: " . $order->price . " $" ?></li>
        </ul>
        <table class="table table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Precio</th>
            </tr>
        </thead>
        <tbody>
        
            <?php foreach ($products as $product){ ?>
                <tr>
                  <td><?php echo $product->findProductName() ?></td>
                  <td><?php echo $product->quantity ?></td>
                  <td><?php echo $product->price . "€" ?></td>
              </tr>
          <?php } ?>
      </tbody>
  </table>
</div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
