<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Cesta</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>

          </tr>
        </thead>
        <tbody>
          <?php foreach ($basket as $p){ ?>
            <tr>
              <td><?php echo $p->name ?></td>
              <td>
                <a href="/order/sumar/<?php echo $p->product_id ?>" class="btn btn-primary">+</a>
                <?php echo $p->quantity ?>
                <a href="/order/bajar/<?php echo $p->product_id ?>" class="btn btn-primary">-</a>
              </td>
              <td><?php echo $p->price . "€" ?></td>
              <td>
               <a class="btn btn-danger" href="/order/remove/<?php echo $p->product_id ?>">Quitar</a>
             </td>
           </tr>
         <?php } ?>
       </tbody>
     </table>
    <hr>
    <a href="/order/store" class="btn">Guardar pedido</a>
  </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
