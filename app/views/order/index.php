<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Mis pedidos</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Fecha</th>
            <th>Precio total</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($orders as $order){ ?>
            <tr>
              <td><?php echo $p->findUserName($order->user_id) ?></td>
              <td><?php echo date("d/m/Y", strtotime($order->date)) ?></td>
              <td><?php echo $p->price . " $" ?></td>
              <td>
                <a class="btn btn-primary" href="/order/show/<?php echo $p->id ?>">Ver</a>
               </td>
           </tr>
         <?php } ?>
       </tbody>
     </table>
  
  </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
