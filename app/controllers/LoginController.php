<?php
namespace App\Controllers;
use App\Models\User;
/**
*
*/
class LoginController
{

    function __construct()
    {
        // echo "En LoginController";
    }

    public function index()
    {
        require "../app/views/logins/index.php";
    }
   
public function login()
{
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];

        // var_dump($email);
        // exit();
        $user = User::find_by_email($email);
        if($user){        
            if(password_verify($password, $user->password)){
                header('Location: /home');
                $_SESSION['user'] = $user;
                return;
            }else{

                header('Location: /login/index');
                $_SESSION['old'] = $email;
                return;
            }        
        }
        header('Location: /login');
        return;


}
public function logout()
{
    unset($_SESSION['user']);
    session_destroy();
    header('Location: /login/index');
}
}
