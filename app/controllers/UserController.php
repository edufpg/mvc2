<?php
namespace App\Controllers;

use App\Models\User;
use Dompdf\Dompdf;
/**
*
*/
class UserController
{

    function __construct()
    {
        // echo "En UserController";
    }
   

    public function index()
    {

        $users = User::all();
        require "../app/views/user/index.php";

    }
    public function show($args)
    {
        $id = (int) $args[0];
        $user = User::find($id);
       
        require('../app/views/user/show.php');
    }public function create()
    {
        require '../app/views/user/create.php';
    }
    
    public function store()
    {
        if($_REQUEST["password"] == $_REQUEST["password2"]){
            $user = new User();
            $user->name = $_REQUEST["name"];
            $user->surname = $_REQUEST["surname"];
            $user->email = $_REQUEST["email"];
            $user->birthdate = date("Y/m/d", strtotime($_REQUEST["birthdate"]));
            $user->setPassword($_REQUEST["password"]);
            $user->insert();

            header("Location:/user");
        }else{
             $_SESSION['old']['name'] = $_REQUEST["name"];
             $_SESSION['old']['surname'] = $_REQUEST["surname"];
             $_SESSION['old']['email'] = $_REQUEST["email"];
             $_SESSION['old']['birthdate'] = $_REQUEST["birthdate"];
             

            $message = "Las contraseñas no coinciden.";
            require "../app/views/user/create.php";
        }
    }
    public function edit($arguments)
{
    $id = (int) $arguments[0];
    $user = User::find($id);
    require '../app/views/user/edit.php';
}

public function update()
{
    $id = $_REQUEST['id'];
    $user = User::find($id);
    $user->name = $_REQUEST['name'];
    $user->surname = $_REQUEST['surname'];
    $user->birthdate = $_REQUEST['birthdate'];
    $user->email = $_REQUEST['email'];
    $user->save();
    header('Location:/user');
}
public function delete($arguments)
{
  $id = (int) $arguments[0];
  $user = User::find($id);
  $user->delete();
  header('Location:/user');
}
public function pdfsimple()
{
    $dompdf = new Dompdf();
$dompdf->loadHtml('hello world');

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
}
public function pdf()
{
    
    ob_start();
    $users = User::all();
    require_once ('../app/views/user/pdf.php');
    $html = ob_get_clean();

    $dompdf = new DOMPDF();
    $dompdf->loadhtml($html);
    $dompdf->render();
    $dompdf->stream("usuarios.pdf",array("Attachment"=>0));

}
}
