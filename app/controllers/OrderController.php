<?php
namespace App\Controllers;
// require_once "../app/models/Basket.php";
// require_once "../app/models/Product.php";
// require_once "../app/models/Order.php";
// require_once "../app/models/OrderProducts.php";
use \App\Models\Basket;
use \App\Models\Product;
use \App\Models\OrderProducts;
use \App\Models\Order;
    /**
    *
    */
    class OrderController
    {
        var $basket;
        function __construct()
        {
        }
        public function añadir($id)
        {
            

            $_SESSION['basket'][$id]= $producto;
        }

        public function index()
        {
            $numero = 5;
            $rowCount = Order::rowCount();
            $orders = Order::paginate($numero);

            $pages = ceil($rowCount / $numero);
            isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;


            require "../app/views/order/index.php";
        }

        public function basket()
        {
            if(isset($_SESSION["basket"])){
                $basket = $_SESSION["basket"];
                
            }else{
                $basket = [];
            }

            $numero = 5;
            $rowCount = count($basket);

            $pages = ceil($rowCount / $numero);
            isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;


            require "../app/views/order/basket.php";
        }

        public function vaciar($args)
        {
            $id = (int)$args[0];
            $product = Product::find($id);
            $basket = $_SESSION["basket"];
            unset($basket[$id]);

            $_SESSION["basket"] = $basket;

            $this->index();
        }

        public function add($args)
        {
            if(isset($_SESSION["basket"])){
                $basket = $_SESSION["basket"];

            }else{
                $basket = [];
            }

            $id = (int)$args[0];
            // $product = Product::find($id);

            if(array_key_exists($id,$basket)){
                $producto = $basket[$id];
               
     
                $producto->quantity = $producto->quantity + 1;
            
            }else{
             
                $producto = Product::find($id);
              
                
                $producto->quantity = 1;
               

            }
            $basket[$id] = $producto;
            $_SESSION["basket"] = $basket;

            header("Location:/product");
        }

        public function sumar($args)
        {
            $id = (int)$args[0];
            $product = Product::find($id);
            $basket = $_SESSION["basket"];
            $producto = $basket[$id];
            $producto->product_id = $producto->product_id;
            $producto->name = $producto->name;
            $producto->quantity = $producto->quantity + 1;
            $producto->price = $producto->price;
            $basket[$id] = $producto;

            $_SESSION["basket"] = $basket;
            $this->basket();
        }

        public function bajar($args)
        {
            $id = (int)$args[0];
            $product = Product::find($id);
            $basket = $_SESSION["basket"];
            $producto = $basket[$id];
            $producto->quantity = $producto->quantity - 1;
            if($producto->quantity == 0){
                unset($basket[$id]);
            }else{
                $producto->product_id = $producto->product_id;
                $producto->name = $producto->name;
                $producto->price = $producto->price;
                $basket[$id] = $producto;
            }

            $_SESSION["basket"] = $basket;
            $this->basket();
        }

        public function store()
        {




            if(isset($_SESSION["basket"]) && !empty($_SESSION["basket"])){
                $basket = $_SESSION['basket'];
                $order = new Order();
                $order->date = date('Y/m/d');
                $user = $_SESSION["user"];
                $order->user_id = $user->id;
                $priceOrder = 0;
                foreach ($basket as $product) {
                    $priceOrder = $priceOrder + $product->price * $product->quantity;
                }
                $order->price = $priceOrder;
                $order->id = $order->insert();

                foreach ($basket as $product) {
                    $order->addproduct($product);
                }
                unset($_SESSION["basket"]);
                $basket = $_SESSION["basket"];
            }

            header('Location:/order');
            // $this->index();
        }

        public function show($args){
            $id = (int)$args[0];
            $order = Order::find($id);
            $products = $order->products();
            require "../app/views/order/show.php";
        }
    }
