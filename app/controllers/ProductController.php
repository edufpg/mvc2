<?php
namespace App\Controllers;

use App\Models\Product;
use App\Models\Producttype;
/**
*
*/
class ProductController
{

    function __construct()
    {
        // echo "En UserController";
    }

    public function index()
    {

        $users = Product::all();
        require "../app/views/products/index.php";

    }
    public function show($args)
    {
        $id = (int) $args[0];
        $user = Product::find($id);
       
        require('../app/views/products/show.php');
    }
    public function delete($arguments)
    {
      $id = (int) $arguments[0];
      $user = Product::find($id);
      $user->delete();
      header('Location:/product');
    }
    public function create()
    {
        $types = Producttype :: all();
        require '../app/views/products/create.php';
    }
    public function store()
    {
        $user = new Product();
        $user->name = $_REQUEST['name'];
        $user->type_id = $_REQUEST['type_id'];
        $user->price = $_REQUEST['price'];
        
        $user->insert();
        header('Location:/product');
    }
    public function edit($arguments)
    {
        $types = Producttype :: all();
        $id = (int) $arguments[0];
        $user = Product::find($id);
        require '../app/views/products/edit.php';
    }
    
    public function update()
    {
        
        $id = $_REQUEST['id'];
        $user = Product::find($id);
        $user->name = $_REQUEST['name'];
        $user->type_id = $_REQUEST['type_id'];
        $user->price = $_REQUEST['price'];
        
        $user->save();
        header('Location:/product');
    }
    
}
