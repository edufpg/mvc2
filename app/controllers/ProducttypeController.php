<?php
namespace App\Controllers;

use App\Models\Producttype;
/**
*
*/
class ProducttypeController
{

    function __construct()
    {
        // echo "En UserController";
    }

    public function index()
    {

       $types = Producttype::all();
        require "../app/views/producttypes/index.php";

    }
    public function show($args)
    {
        $id = (int) $args[0];
        $type = Producttype::find($id);
       
        require('../app/views/producttypes/show.php');
    }
    public function delete($arguments)
    {
      $id = (int) $arguments[0];
      $type = Producttype::find($id);
      $type->delete();
      header('Location:/producttype');
    }
    public function store()
    {
        $type = new Producttype();
        $type->name = $_REQUEST['name'];
        
        $type->insert();
        header('Location:/producttype');
    }
    public function edit($arguments)
    {
        $id = (int) $arguments[0];
        $type = Producttype::find($id);
        require '../app/views/producttypes/edit.php';
    }
    public function update()
    {
        $id = $_REQUEST['id'];
        $type = Producttype::find($id);
        $type->name = $_REQUEST['name'];
        
        $type->save();
        header('Location:/producttype');
    }
    public function create()
    {
        $type = Producttype::all();
        require '../app/views/producttypes/create.php';
    }
}
