<?php

namespace App\Controllers;
// require_once "../app/models/User.php";
use \App\Models\User;
    /**
    *
    */
    class RegisterController
    {

        function __construct()
        {
            // if (! isset($_SESSION['user'])) {
            //     header("Location:/login");
            //     return;
            // }
        }

        public function index(){
            require "../app/views/register/index.php";
        }

        public function store(){

            if($_REQUEST["password"] == $_REQUEST["password2"]){
                $user = new User();
                $user->name = $_REQUEST["nombre"];
                $user->surname = $_REQUEST["apellidos"];
                $user->email = $_REQUEST["email"];
                $user->birthdate = date("Y/m/d", strtotime($_REQUEST["nacimiento"]));
                $user->setPassword($_REQUEST["password"]);
                $user->insert();

                header("Location:/user");
            }else{
                $message = "Las contraseñas no coinciden.";
                require "../app/views/register/index.php";
            }


        }
    }
